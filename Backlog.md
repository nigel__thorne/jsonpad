Ideas:
* Show JsonPath Cheatsheet
* Breadcrums -- so you can see where you are in a document
* Collapse all -- so you can navigate big documents easier
* Clickable breadcrums -- to jump to outer scopes easier.
* Keyboard shortcut to collapse current object
* Keyboard shortcut to expand current line
* automatic refresh output as expression changes
* automatic refresh output as source document changes
* automatic reload source document when its' a file
* automatic polling source document when its' a url
* configure polling rate for urls
* remember past document list
* remember expressions history across reloads
