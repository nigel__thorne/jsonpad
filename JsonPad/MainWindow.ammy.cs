﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Navigation;
using AvalonEdit.Sample;
using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Folding;
using ICSharpCode.AvalonEdit.Rendering;
using ICSharpCode.AvalonEdit.Search;
using Microsoft.Win32;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ServiceStack;

namespace JsonPad
{
    public partial class MainWindow
    {
        public static readonly RoutedCommand LoadJson = new RoutedCommand();
        public static readonly RoutedCommand FormatJson = new RoutedCommand();
        private List<string> _history = new List<string>();

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = new MyViewModel();
            jsonExpression.Text = "$.Manufacturers..[?(@Products[?(@Name=='Elbow Grease')])]";
            textEditor.Text =
                @"{
  'Stores': [
    'Lambton Quay',
    'Willis Street'
  ],
  'Manufacturers': [
    {
      'Name': 'Acme Co',
      'Products': [
        {
          'Name': 'Anvil',
          'Price': 50
        }
      ]
    },
    {
      'Name': 'Contoso',
      'Products': [
        {
          'Name': 'Elbow Grease',
          'Price': 99.95
        },
        {
          'Name': 'Headlight Fluid',
          'Price': 4
        }
      ]
    },
    {
      'Name': 'Judith Co.',
      'Products': [
        {
          'Name': 'Being Awesome',
          'Price': 99.95
        },
        {
          'Name': 'Keeping Nigel in Check...',
          'Price': 0.0
        }
      ]
    }
  ]
}";

            ConfigureForJson(textEditor);
            ConfigureForJson(outputEditor);
            documentLoader.OnDocumentLoaded += DocumentLoaderOnOnDocumentLoaded; 
            documentLoader.OnError += DocumentLoaderOnOnError;
            RunExpression();
            this.AddHandler(Hyperlink.RequestNavigateEvent,
                new RequestNavigateEventHandler(RequestNavigateHandler));

            textEditor.ShowLineNumbers = true;
        }

        private void RequestNavigateHandler(object sender, RequestNavigateEventArgs e)
        {
            documentLoader.Open(e.Uri.AbsoluteUri);
            e.Handled = true;
        }

        private void DocumentLoaderOnOnError(object sender, OnLoadErrorEventHandlerArgs args)
        {
            outputEditor.Text = args.Text;
        }

        private void DocumentLoaderOnOnDocumentLoaded(object sender, DocumentLoadedEventArgs args)
        {
            textEditor.Text = IndentJson(args.Text);
            RunExpression();
        }

        private void ConfigureForJson(TextEditor editor)
        {
            var foldingManager = FoldingManager.Install(editor.TextArea);
            var braceFoldingStrategy = new BraceFoldingStrategy();
            braceFoldingStrategy.UpdateFoldings(foldingManager, editor.Document);
            editor.TextChanged += (sender, args) =>
            {
                braceFoldingStrategy.UpdateFoldings(foldingManager, editor.Document);

            };
            SearchPanel.Install(editor);
        }

        private void RunExpression()
        {
            try
            {
                bool errorOnNoMatch = false;
                var jToken = EvaluateExpression(jsonExpression.Text, textEditor.Text, errorOnNoMatch);
                outputEditor.Text = RenderTokens(jToken);

                try
                {
                    if (jToken != null && jToken.HasValues)
                    {
                        EvaluateExpression(jsonExpression.Text, textEditor.Text, errorOnNoMatch: true);
                    }
                }
                catch
                {
                    // Jtoken has values and EvalExpression throws an error... this means the
                    // jsonpath is only valid for some subset of the matches nodes.
                    outputEditor.Text = $"//** This is an INVALID QUERY if you have ErrorOnNoMatch turned on. **\n" +
                                        $"// This happens when you filter to a list of results, \n" +
                                        $"//      then access a property, but that property is only present on some of those matches.\n" +
                                        $"// eg.  [?(some filter)].payload.duration  =>  This will fail if not all payloads have a duration. \n" +
                                        $"// \n" +
                                        $"// You can get around this by filtering to only those results that have a duration \n" +
                                        $"// * Match only nodes that have a duration in their payload\n" +
                                        $"// eg. [?(some filter  && @.payload.duration)].payload.duration \n" +
                                        $"// * Re-filter the nodes before accessing duration" +
                                        $"// eg. [?(some filter)].payload><[?(@duration)].duration\n" +
                                        $"// * In some situations you can _search_ for the duration\n" +
                                        $"// eg. [?(some filter)].payload..duration\n" +
                                        $"//    BUT this will match duration in all child nodes too, so may give different results!\n" +
                                        $"\n" +
                                        outputEditor.Text;
                }
                _history.Add(jsonExpression.Text);
                _history = _history.Distinct().ToList();
            }
            catch (Exception e)
            {
                outputEditor.Text = e.Message;
            }
        }

        private static JToken EvaluateExpression(string expression, string json, bool errorOnNoMatch)
        {
            var jToken = JToken.Parse(json);
            return Regex.Split(expression, @"><").Aggregate(jToken, (token, s) => SelectTokens(token, s, errorOnNoMatch));
        }

        public void ShowText(string text)
        {
            outputEditor.Text = text;
        }

        private static JArray SelectTokens(JToken text, string expression, bool errorWhenNoMatch)
        {
            return new JArray(text.SelectTokens(expression, errorWhenNoMatch));
        }

        private static string RenderTokens(IEnumerable<JToken> tokens)
        {
            return string.Join("\n// --------------------------------------------\n\n",
                tokens.ToList().Select((token) => token.ToString(Newtonsoft.Json.Formatting.Indented)));
        }

        private void OnRunClicked(object sender, RoutedEventArgs e)
        {
            RunExpression();
        }

        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                // Whatever code you want if enter key is pressed goes here
                RunExpression();
            }

            if (e.Key == Key.Up)
            {
                var index = _history.LastIndexOf(jsonExpression.Text);
                if (index - 1 >= 0)
                    jsonExpression.Text = _history[index - 1];
            }

            if (e.Key == Key.Down)
            {
                var index = _history.LastIndexOf(jsonExpression.Text);
                if (index + 1 < _history.Count)
                    jsonExpression.Text = _history[index + 1];
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control)
            {
                if (e.KeyboardDevice.GetKeyStates(Key.E) == KeyStates.Down)
                    jsonExpression.Focus();
            }
        }

        public void LoadJson_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        public void LoadJson_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            documentLoader.PromptUser();
        }

        public void FormatJson_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        public void FormatJson_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            textEditor.Text = IndentJson(textEditor.Text);
        }



        private string IndentJson(string read_all_text)
        {
            return JToken.Parse(read_all_text).ToString(Formatting.Indented);
        }


    }
}