﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using AmmySidekick;

namespace JsonPad
{
    public partial class App : Application
    {
        [STAThread]
        public static void Main()
        {
            App app = new App();
            app.DispatcherUnhandledException += app.UnhandledException;
            app.InitializeComponent();

            RuntimeUpdateHandler.Register(app, "/" + AmmySidekick.Ammy.GetAssemblyName(app) + ";component/App.g.xaml");
            app.Run();
        }

        public void UnhandledException ( object sender, DispatcherUnhandledExceptionEventArgs e )
        {
            ((MainWindow)MainWindow).ShowText ( e.Exception.Message );
            e.Handled = true;
        }
    }
}
