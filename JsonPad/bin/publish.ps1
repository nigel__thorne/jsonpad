$sln = Get-Content "${PSScriptRoot}\..\Properties\AssemblyInfo.cs"
$version = $sln | Select-String '^\[assembly: AssemblyVersion\(\s*"(?<version>[0-9]+(?:\.[0-9]+)*)"\s*\)\]\s*$' | Foreach-Object {$_.Matches} | Foreach-Object {$_.Groups['version'].value}
pushd ${PSScriptRoot}\Release
echo "Generating bin\JsonPad_v${version}.zip"
zip "..\JsonPad_v${version}.zip" -r . -q
popd
