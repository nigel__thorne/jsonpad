﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Win32;
using ServiceStack;

namespace JsonPad
{
    public class DocumentLoadedEventArgs
    {
        public DocumentLoadedEventArgs(string s) { Text = s; }
        public String Text { get; } // readonly
    }
    public class OnLoadErrorEventHandlerArgs
    {
        public OnLoadErrorEventHandlerArgs(string s) { Text = s; }
        public String Text { get; } // readonly
    }


    public partial class DocumentLoader
    {
        private Stack<string> _navigationStack;

        public delegate void DocumentLoadedEventHandler(object sender, DocumentLoadedEventArgs args);
        public delegate void OnLoadErrorEventHandler(object sender, OnLoadErrorEventHandlerArgs args);

        public event DocumentLoadedEventHandler OnDocumentLoaded;
        public event OnLoadErrorEventHandler OnError;

        public DocumentLoader()
        {
            _navigationStack = new Stack<string>();
            InitializeComponent();
        }

        private void OpenFileDialog()
        {
            var openFileDialog = new OpenFileDialog { Filter = "JSON Files (*.json)|*.json|All Files (*.*)|*.*" };
            if (openFileDialog.ShowDialog() != true) return;
            Open(openFileDialog.FileName);
        }
        private void OnClearClicked(object sender, RoutedEventArgs e)
        {
            this.password.Text = "";
            this.username.Text = "";
        }
        private void OnLoadClicked(object sender, RoutedEventArgs e)
        {
            Load();
        }

        private void Load()
        {
            var location = loadpath.Text;
            if (String.IsNullOrEmpty(location))
            {
                OpenFileDialog();
                return;
            }

            try
            {
                if (new Regex("https?://").IsMatch(location))
                {
                    var uri = new Uri(location);

                    var parts = uri.UserInfo.Split(':');

                    if (!string.IsNullOrEmpty(uri.UserInfo))
                    {
                        if (parts.Length > 0)
                            username.Text = parts[0];

                        if (parts.Length > 1)
                            password.Text = parts[1];

                        loadpath.Text = uri.GetComponents(UriComponents.AbsoluteUri & ~UriComponents.UserInfo,
                            UriFormat.UriEscaped);
                    }

                    var client = new JsonServiceClient(uri.GetLeftPart(System.UriPartial.Authority));

                    if (!string.IsNullOrEmpty(username.Text)) client.UserName = username.Text;
                    if (!string.IsNullOrEmpty(password.Text)) client.Password = password.Text;
                    client.AlwaysSendBasicAuthHeader = true;

                    var response = client.Get<string>(uri.PathAndQuery);
                    OnDocumentLoaded?.Invoke(this, new DocumentLoadedEventArgs(response));
                    PushLocation(location);
                }
                else
                {
                    OnDocumentLoaded?.Invoke(this, new DocumentLoadedEventArgs(File.ReadAllText(location)));
                    PushLocation(location);
                }
            }
            catch (Exception x)
            {
                OnError?.Invoke(this, new OnLoadErrorEventHandlerArgs(x.ToString()));
            }
        }

        private void PushLocation(string location)
        {
            _navigationStack.Push(location);
        }

        public void PromptUser()
        {
            OpenFileDialog();
        }

        public void Open(string location)
        {
            loadpath.Text = location;
            Load();
        }

        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void OnBackClicked(object sender, RoutedEventArgs e)
        {
            var previous = loadpath.Text;
            while (_navigationStack.Any())
            {
                var location = _navigationStack.Pop();
                if (previous != location)
                {
                    Open(location);
                    return;
                }
            }
        }
    }
}
