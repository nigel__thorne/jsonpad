﻿using System.Reflection;
using Newtonsoft.Json.Linq;

namespace JsonPad
{
    public class MyViewModel
    {
        public string AppVersion()
        {
            return Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public string ParserVersion()
        {
            return typeof(JToken).Assembly.GetName().Version.ToString();
        }
    }
}